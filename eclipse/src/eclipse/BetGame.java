package eclipse;


import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/*
 * BetGame is an application that creates a betting game of heads or tails.
 * @author David Nguyen
 */
public class BetGame extends Application{
	
	/**
	 * start sets up the scene
	 * @param Stage stage
	 */
	public void start(Stage stage){
		
		//creates a new user to store all info
		User currentUser = new User();
		
		final int WIDTH = 300;
		final int HEIGHT = 350;
		Group root = new Group();
		
		Scene scene = new Scene(root,WIDTH,HEIGHT);
		scene.setFill(Color.ALICEBLUE);
		
		//creates a new gametab which creates the betting game
		GameTab gameTab = new GameTab(currentUser);
		TabPane tb = new TabPane();
		
		//add tab to tabpane
		tb.getTabs().add(gameTab);
		root.getChildren().add(tb);
		
		//show scene
		stage.setScene(scene);
		stage.show();

	}
	/**
	 * main launches the application
	 * @param args
	 */
	public static void main(String[]args) {
		Application.launch(args);
	}
}