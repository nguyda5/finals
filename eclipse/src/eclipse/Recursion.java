package eclipse;

public class Recursion {
	/**
	 * @author David Nguyen
	 * @param numbers an array of numbers
	 * @param n a number
	 * @return the number of ints that respect all conditions
	 */
	public static int recursiveCount(int[] numbers, int n) {
		int count = 0;
		//once n has reach past the last index, return 0
		//this means the recursion has ended
		if(n > numbers.length - 1) {
			return count;
		}
		//if number respects condition, adds 1 and the next value of the recursion
		if(numbers[n] > 20 && n % 2 == 1) {
			count+= 1 + recursiveCount(numbers, n + 1);
		}else {
		//if number does not respect condition, adds nothing and calls recursion
			count+= 0 + recursiveCount(numbers, n + 1);
		}
		//once the method has ended, it returns the count depending on its n
		return count;
		
	}
	public static void main(String[] args) {
		int[] test = new int[] {50,20,30,22,64,27,2,82};
		int nTest = 3;
		System.out.println(recursiveCount(test,nTest));
		//expected result 3
		test = new int[] {23,53,2,3,2,5,46,54,20,1};
		nTest = 5;
		System.out.println(recursiveCount(test,nTest));
		//expected result 1
		
	}

}
