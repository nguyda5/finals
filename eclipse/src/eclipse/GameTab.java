package eclipse;


import java.util.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.layout.*;

/*
 * GameTab creates the betting game as a tab.
 * @author David Nguyen
 */
public class GameTab extends Tab{
	User currentUser;
	GridPane gamePane;
	Label displayCash;
	Label displayBetText;
	Label displayBetChoice;
	Label displayInsertAmount;
	Label messageToUser;
	TextField insertAmount;
	Button betHeads;
	Button betTails;
	Button startBet;
	
	/*
	 * GameTab constructor that initializes the betting game
	 */
	public GameTab(User currentUser) {
		super("Betting Game");
		this.currentUser = currentUser;
		initiateComponents();
		setupComponents();
		addChoiceEvents();
		addBetEvent();
		this.setContent(gamePane);
	}
	/*
	 * initiates all javafx components
	 */
	public void initiateComponents() {
		gamePane = new GridPane();
		displayCash = new Label();
		displayBetText = new Label();
		displayBetChoice = new Label();
		displayInsertAmount = new Label();
		messageToUser = new Label();
		insertAmount = new TextField();
		betHeads = new Button();
		betTails = new Button();
		startBet = new Button();
	}
	/*
	 * setupComponents sets up all the components and adds them to the game pane
	 */
	public void setupComponents() {
		setupLabels();
		setupButtons();
		gamePane.add(displayCash, 1, 1);
		gamePane.add(betHeads, 1, 2);
		gamePane.add(betTails, 2, 2);
		gamePane.add(displayBetText, 1, 3);
		gamePane.add(displayBetChoice, 2, 3);
		gamePane.add(displayInsertAmount, 1, 4);
		gamePane.add(insertAmount, 2, 4);
		gamePane.add(startBet, 1, 5);
		gamePane.add(messageToUser, 2, 5);
		gamePane.setHgap(20);
		gamePane.setVgap(20);
	}
	/*
	 * setupLabels sets up all the labels
	 */
	public void setupLabels() {
		displayCash.setText("Cash : "+currentUser.getCash()+"$");
		displayBetText.setText("Bet is on: ");
		displayBetChoice.setText(currentUser.getBetChoice());
		displayInsertAmount.setText("Insert amount: ");
		messageToUser.setText("");
	}
	/*
	 * setupButtons sets up all the buttons
	 */
	public void setupButtons() {
		betHeads.setText("Heads");
		betTails.setText("Tails");
		startBet.setText("Bet");
	}
	/*
	 * addChoiceEvents adds an event handler to the bet button and heads button
	 * If heads is clicked, the user choice will be changed to heads
	 * If tails is clicked, the user choice will be changed to tails
	 */
	public void addChoiceEvents() {
		betHeads.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent e) {
				currentUser.setBetChoice("Heads");
				//calls refresh to refresh the ui to properly display the information
				refresh();
			}
			
		});
		betTails.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent e) {
				currentUser.setBetChoice("Tails");
				//calls refresh to refresh the ui to properly display the information
				refresh();
			}
			
		});
	}
	/*
	 * addBetEvent adds the bet event so the user can start the bet and see if they won or not
	 */
	public void addBetEvent() {
		startBet.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent e) {
				//first verifies if the textfield for insertAmount contains only numbers
				if(containsOnlyNumbers()) {
					//second verifies if the amount of cash inserted is above the user's current cash
					if(!isAboveCash()) {
						//get result if its heads or tails
						String result = headsOrTails();
						//if user won, displays result
						if(result.equals(currentUser.getBetChoice())) {
							messageToUser.setText("Won: " + result);
						}else {
							//if user lost, displays result and substracts from user's cash
							currentUser.setCash(currentUser.getCash() - Integer.parseInt((insertAmount.getText())));
							messageToUser.setText("Lost: " + result);
						}	
					}
				}
				//at the end, refreshes the gui to properly display the information
				refresh();
			}
			
		});
	}
	/*
	 * isAboveCash verifies if the cash inserted is above the user's current cash
	 */
	public boolean isAboveCash() {
		int cashBet = Integer.parseInt((insertAmount.getText()));
		if(cashBet > currentUser.getCash()) {
			messageToUser.setText("Not enough!");
			return true;
		}
		return false;
		
	}
	/*
	 * containsOnlyNumbers verifies if the cash inserted contains only numbers
	 */
	public boolean containsOnlyNumbers() {
		String cashBet = insertAmount.getText();
		if(!cashBet.matches("[0-9]+")) {
			messageToUser.setText("Numbers only!");
			return false;
		}
		return true;
	}
	/*
	 * headsOrTails returns a result between heads or tails
	 */
	public String headsOrTails() {
		Random rng = new Random();
		int resultNum = rng.nextInt(2);
		String result = "";
		if(resultNum == 0) {
			result = "Heads";
		}else {
			result = "Tails";
		}
		return result;
	}
	/*
	 * refresh refreshes the ui to display the appropriate information
	 */
	public void refresh() {
		displayCash.setText("Cash : "+currentUser.getCash()+"$");
		displayBetChoice.setText(currentUser.getBetChoice());
	}
	
}
