package eclipse;

/**
 * User object which represents a user
 * @author David
 *
 */
public class User {
	private int cash;
	private String betChoice;
	/*
	 * User constructor that gives 100 cash to the user and assign a "none" bet choice.
	 */
	public User() {
		this.cash = 100;
		this.betChoice = "none";
	}
	public String getBetChoice() {
		return betChoice;
	}
	public void setBetChoice(String betChoice) {
		this.betChoice = betChoice;
	}
	public int getCash() {
		return cash;
	}
	public void setCash(int cash) {
		this.cash = cash;
	}
	
}
